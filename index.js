let posts = [];

let count = 1;


	document.querySelector("#form-add-post").addEventListener("submit", (event)=>{
		event.preventDefault();
		let newPost = {
			id:count,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value
		}
		

		posts.push(newPost);

		console.log(posts);
		count++;
		showPosts(posts);
	})

	//SECTION - Show posts

	const showPosts = (posts)=>{
		 let postEntries = ``;

		 posts.forEach((post)=>{
		 	postEntries += `
		 		<div id="post-${post.id}">
		 			<h3 id="post-title-${post.id}">${post.title}</h3>
		 			<p id="post-body-${post.id}">${post.body}</p>
		 			<button onclick = "editPost(${post.id})">Edit</button>
		 			<button onclick = "deletePost(${post.id})">Delete</button>
		 		</div>
		 	`
		 })
		 document.querySelector("#div-post-entries").innerHTML = postEntries;
	}


// EDIT POST
	
	const editPost = (id) =>{
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		console.log(title);
		console.log(body);

		document.querySelector("#txt-edit-id").value = id;
		document.querySelector("#txt-edit-title").value = title;
		document.querySelector("#txt-edit-body").value = body;
	}


	document.querySelector("#form-edit-post").addEventListener("submit", (event) => {
		event.preventDefault();


		//forEach to check/find the document to be edited

		posts.forEach(post => {
			let idToBeEdited = document.querySelector("#txt-edit-id").value;

			console.log(typeof idToBeEdited);
			console.log(typeof post.id);
			if(post.id == idToBeEdited){
				let title = document.querySelector("#txt-edit-title").value;
				let body = document.querySelector('#txt-edit-body').value;


				posts[post.id-1].title = title;
				posts[post.id-1].body = body;

				alert("Edit is successful!");
				showPosts(posts);

				document.querySelector("#txt-edit-title").value = "";
				document.querySelector('#txt-edit-body').value = "";
				document.querySelector('#txt-edit-id').value = "";

 			}
		})


	})


//ACTIVITY S48
	let deletePost = (id) =>{

	            const i = posts.findIndex((index) => index.id === id)
	            if(i > -1){
	                posts.splice(i, 1)
	            }
	           document.querySelector(`#post-${id}`).innerHTML = "";
	    }